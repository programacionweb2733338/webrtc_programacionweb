import firebase from "firebase/app";
import 'firebase/database';

var firebaseConfig = {
  apiKey: 'AIzaSyBdluyDcMHZ_PH5UrUgCYd1ybdf7ifKr7I',
  databaseURL: 'https://webrtc-2f119-default-rtdb.firebaseio.com/',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase;

var firepadRef = firebase.database().ref();

export const userName = prompt("What's your name?");
const urlparams = new URLSearchParams(window.location.search);
const roomId = urlparams.get("id");

if (roomId) {
  firepadRef = firepadRef.child(roomId);
} else {
  firepadRef = firepadRef.push();
  window.history.replaceState(null, "Meet", "?id=" + firepadRef.key);
}

export default firepadRef;
