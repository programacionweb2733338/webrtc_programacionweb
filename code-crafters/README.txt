## Proyecto WEBRTC

## Estructura del Proyecto

El proyecto sigue la estructura estándar de una aplicación 
creada con Create React App. Los archivos de código fuente se 
encuentran en la carpeta src/, mientras que los archivos estáticos
públicos se encuentran en la carpeta public/.


## Componentes

Los componentes de React se encuentran en la carpeta src/components/. 
Cada componente tiene su propio archivo .js y .css. Los componentes principales son:


## MainScreen

Este es probablemente el componente principal de la aplicación, 
donde se muestra la pantalla de la reunión.


## MeetingFooter

Este componente podría ser la barra de herramientas que se muestra 
en la parte inferior de la pantalla durante una reunión.


## Participants

Este componente podría ser la lista de participantes en una reunión.


## Servidor

Los archivos relacionados con la configuración del servidor se encuentran en 
la carpeta src/server/. Los archivos principales son:


## firebase.js

Este archivo configura Firebase, que se utiliza para la base de 
datos en tiempo real y posiblemente para la autenticación de usuarios.
peerConnection.js: Este archivo podría manejar la conexión entre pares para las 
videoconferencias.


## Store

Los archivos relacionados con Redux se encuentran en la carpeta src/store/. 
Estos archivos manejan el estado global de la aplicación.



## Recomendaciones y uso del proyecto

- Utilizar node v14.18.1.
- Si ya tienes una version de node instalada utiliza nvm para instalar una nueva version.
- Instalar npm con el comando: "npm Install".
- Añadir nuestra configuracion de firebase en la ruta: Server/firebase.js 
(este proyecto actualmente ya lo tiene configurado).
- Para ejecutar el proyecto correr el comando "npm run start".


## Informacion del grupo

* Grupo : Codecrafters
* Integrantes:
	- Wally Kevin Zeballos Oquendo
	- Mairon Henry Fernandez Orellana
